/*
  FLUID VIDEO
  Make youtube, vimeo & google maps responsive
  Dependencies: Fitvids.js
*/

$(document).ready(function(){

  $('html').fitVids();

});
