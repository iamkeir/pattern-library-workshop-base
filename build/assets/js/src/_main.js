/*
  MAIN JS
  Global, non-modular stuff
*/

// ON READY
$(document).ready(function(){

  console.log('document.ready');

});

// ON LOAD
$(window).on('load', function() {

  console.log('window.loaded');

});
