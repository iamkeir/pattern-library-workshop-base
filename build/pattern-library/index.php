<!doctype html>
<html class="no-js" lang="en">
<head>

  <!-- .no-js class switcher -->
  <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1" />

  <title>Pattern Library</title>

  <meta name="description" content="" />
  <meta name="keywords" content="" />

  <!-- Favicons -->
  <link rel="shortcut icon" href="/favicon.png" />
  <link rel="apple-touch-icon" href="/apple-touch-icon.png" />

  <link href="/assets/css/style.min.css" rel="stylesheet" type="text/css" />

</head>

<body>

  <?php if (@$_GET['template']) : ?>

    <!-- TEMPLATE -->
    <?php include 'templates/'.$_GET['template'].'.inc.php'; ?>

  <?php elseif (@$_GET['module']) : ?>

    <!-- MODULE -->
    <?php include 'modules/'.$_GET['module'].'.inc.php'; ?>

  <?php elseif (@$_GET['core']) : ?>

    <!-- CORE -->
    <?php include 'core/'.$_GET['core'].'.inc.php'; ?>

  <?php else : ?>

    <!-- INDEX -->
    <div class="_pl-index">

      <h1>Pattern Library</h1>

      <hr />

      <div class="_pl-grid">
        <div class="_pl-grid-item">

          <h2>Core</h2>

          <ul>
            <li><a href="?core=typography">Typography</a></li>
            <li><a href="?core=media">Media</a></li>
            <li><a href="?core=button">Button</a></li>
            <li><a href="?core=layout">Layout</a></li>
          </ul>

        </div>
        <div class="_pl-grid-item">

          <h3>Modules</h3>

          <ul>
            <li><a href="?module=footer">Footer</a></li>
          </ul>

        </div>
        <div class="_pl-grid-item">

          <h3>Templates</h3>

          <ul>
            <li><a href="?template=page">Page structure</a></li>
          </ul>

        </div>
      </div><!--/_pl-index__grid-->

    </div><!--/_pl-index-->

    <style>
      ._pl-index { max-width: 1024px; margin: 0 auto; padding: 0 20px; }
      ._pl-grid { display: flex; flex-wrap: wrap; margin: 0 -10px;}
      ._pl-grid-item { width: calc(100%/3); padding: 0 10px; }
    </style>

  <? endif; ?>

  <script src="/assets/js/main.min.js"></script>

</body>
</html>
