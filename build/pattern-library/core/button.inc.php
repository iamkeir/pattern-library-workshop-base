<p>
  <a class="btn" href="#">.btn</a>
  <button class="btn">.btn</button>
</p>

<p>
  <a class="btn btn--2" href="#">.btn.btn--2</a>
  <button class="btn btn--2">.btn.btn--2</button>
</p>

<p>
  <a class="btn btn--2" disabled>.btn[disabled]</a>
  <button class="btn btn--2" disabled>.btn[disabled]</button>
</p>
