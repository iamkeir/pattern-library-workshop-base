<style>
  code {
    display: block;
    background: #aaa;
    color: white;
    margin: 5px 0;
    text-align: center;
    font-size: 14px;
  }
</style>

<div class="layout"><code>.layout</code></div>
<div class="layout layout--nogutter"><code>.layout--nogutter</code></div>

<br />

<div class="layout layout--reduced"><code>.layout--reduced</code></div>
<div class="layout layout--wide"><code>.layout--wide</code></div>
<div class="layout layout--full"><code>.layout--full</code></div>
