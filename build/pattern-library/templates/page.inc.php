<div class="page">

  <header class="page__header">
    <div class="layout">
      HEADER
    </div>
  </header>

  <main class="page__main">
    <div class="layout">
      CONTENT
    </div>
  </main>

  <footer class="page__footer">
    <div class="layout">
      FOOTER
    </div>
  </footer>

</div><!--/page-->
