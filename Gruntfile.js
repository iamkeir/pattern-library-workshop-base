/*
  GRUNT ALL TEH THINGS
*/

'use strict';

module.exports = function(grunt) {

  // PLUGINS
  grunt.loadNpmTasks('grunt-sass'); // Libsass compiling
  grunt.loadNpmTasks('grunt-autoprefixer'); // Browser Autoprefixing
  grunt.loadNpmTasks('grunt-contrib-cssmin'); // CSS minifying
  grunt.loadNpmTasks('grunt-contrib-concat'); // JS concatenating
  grunt.loadNpmTasks('grunt-contrib-uglify'); // JS minifying
  grunt.loadNpmTasks('grunt-contrib-watch'); // Watch for changes
  grunt.loadNpmTasks('grunt-notify'); // Notify of change

  // BUILD TASKS
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // Compile Sass
    sass: {
      dist: {
        files: {
          'build/assets/css/style.css': 'build/assets/scss/style.scss' // destination - source file
        }
      }
    },

    // Autoprefixing for older browsers
    autoprefixer: {
      options: {
        browsers: ['last 2 versions', 'ie 9']
      },
      dist: {
        src: ['build/assets/css/style.css']
      }
    },

    // Minify CSS
    cssmin: {
      dist: {
        src: 'build/assets/css/style.css',
        dest: 'build/assets/css/style.min.css'
      }
    },

    // Concatenate JS
    concat: {
      dist: {
        src: [
          'build/assets/js/src/_jquery-*',
          'build/assets/js/src/vendor/**/*',
          'build/assets/js/src/modules/**/*',
          'build/assets/js/src/_main.js'
        ],
        dest: 'build/assets/js/main.js'
      }
    },

    // Minify JS
    uglify: {
      dist: {
        files: {
          'build/assets/js/main.min.js': ['build/assets/js/main.js']
        }
      }
    },

    // Watch for changes
    watch: {
      options: {
        spawn: false
      },
      css: {
        files: ['build/assets/scss/**/*.scss'],
        tasks: ['css']
      },
      js: {
        files: ['build/assets/js/src/**/*.js'],
        tasks: ['js']
      }
    }

  });

  // CUSTOM TASKS
  grunt.registerTask('default', ['css', 'js', 'watch']); // process assets & watch for changes
  grunt.registerTask('css', ['sass', 'autoprefixer', 'cssmin']); // process all CSS
  grunt.registerTask('js', ['concat', 'uglify']); // process all JS

};
