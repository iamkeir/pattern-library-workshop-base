# BASE

## Explanation

A base architecture for building a basic pattern library, using PHP for includes/variables and Grunt for asset processing.

## Getting started

1. Clone repository: https://bitbucket.org/iamkeir/pattern-library-workshop-base
2. Open Terminal and navigate to project directory `cd path/to/project`
3. Check if we have Node installed `node -v`
4. If not, download and install Node: https://nodejs.org/en/download/
5. Ensure latest version of NPM `sudo npm install -g` and then check version `npm -v`
6. Install project dependencies `npm install`
7. Install Grunt CLI globally `sudo npm install -g grunt-cli` and then check version `grunt -version`
8. Run `grunt` to begin processing and watching assets
9. Install MAMP (https://www.mamp.info/en/) and setup localhost to point to `build/`
10. Go to http://localhost/ in browser of choice

_NOTE: To stop Grunt running, use `ctrl + c`_

## Technology

- The site runs on PHP via MAMP but any PHP stack will work.
- Grunt is used for processing Sass (SCSS) and JavaScript.
